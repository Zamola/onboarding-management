package zamola.onboardingmanagement;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Set;

@RequiredArgsConstructor
public abstract class RestController<Entity> {
    private final Service<Entity> service;

    @PostMapping
    @PutMapping
    public void createOrUpdate(@RequestBody final Entity entity) {
        service.createOrUpdate(entity);
    }

    @GetMapping
    public Set<Entity> readAll() {
        return service.readAll();
    }

    @GetMapping("/{id}")
    public Entity read(@PathVariable final long id) {
        return service.read(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final long id) {
        service.delete(id);
    }
}
