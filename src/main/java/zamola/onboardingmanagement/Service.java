package zamola.onboardingmanagement;

import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public abstract class Service<Entity> {
    private final Repository<Entity> repository;

    public void createOrUpdate(final Entity entity) {
        repository.createOrUpdate(entity);
    }

    public Set<Entity> readAll() {
        return repository.readAll();
    }

    public Entity read(final long id) {
        return repository.read(id);
    }

    public void delete(final long id) {
        repository.delete(id);
    }
}
