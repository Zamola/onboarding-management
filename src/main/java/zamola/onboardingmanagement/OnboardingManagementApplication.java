package zamola.onboardingmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnboardingManagementApplication {
    public static void main(final String[] args) {
        SpringApplication.run(OnboardingManagementApplication.class, args);
    }
}
