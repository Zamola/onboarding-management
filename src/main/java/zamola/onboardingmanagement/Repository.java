package zamola.onboardingmanagement;

import java.util.Set;

public interface Repository<Entity> {
    void createOrUpdate(Entity entity);

    Set<Entity> readAll();

    Entity read(long id);

    void delete(long id);
}
