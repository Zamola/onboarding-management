package zamola.onboardingmanagement.security.privileges;

import zamola.onboardingmanagement.Repository;

public interface PrivilegeRepository extends Repository<Privilege> {
}
