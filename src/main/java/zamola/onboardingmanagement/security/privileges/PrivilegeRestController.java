package zamola.onboardingmanagement.security.privileges;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zamola.onboardingmanagement.Service;

@RestController
@RequestMapping("/privileges")
public class PrivilegeRestController extends zamola.onboardingmanagement.RestController<Privilege> {
    public PrivilegeRestController(final Service<Privilege> service) {
        super(service);
    }
}
