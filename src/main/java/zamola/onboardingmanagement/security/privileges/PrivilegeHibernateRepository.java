package zamola.onboardingmanagement.security.privileges;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class PrivilegeHibernateRepository implements PrivilegeRepository {
    private static final String ID = "id";

    private final EntityManager entityManager;

    @Override
    public void createOrUpdate(final Privilege privilege) {
        final Session session = getSession();
        session.saveOrUpdate(privilege);
    }

    @Override
    public Set<Privilege> readAll() {
        final Session session = getSession();
        final Query<Privilege> query = session.createQuery("from Privilege", Privilege.class);
        return query.getResultStream().collect(Collectors.toSet());
    }

    @Override
    public Privilege read(final long id) {
        final Session session = getSession();
        return session.get(Privilege.class, id);
    }

    @Override
    public void delete(final long id) {
        final Session session = getSession();
        final Query query = session.createQuery("delete from Privilege where id=:" + ID);
        query.setParameter(ID, id);
        query.executeUpdate();
    }

    private Session getSession() {
        return entityManager.unwrap(Session.class);
    }
}
