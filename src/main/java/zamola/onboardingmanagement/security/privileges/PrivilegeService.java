package zamola.onboardingmanagement.security.privileges;

import org.springframework.stereotype.Service;
import zamola.onboardingmanagement.Repository;

@Service
public class PrivilegeService extends zamola.onboardingmanagement.Service<Privilege> {
    public PrivilegeService(final Repository<Privilege> repository) {
        super(repository);
    }
}
