package zamola.onboardingmanagement.security.users;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zamola.onboardingmanagement.Service;

@RestController
@RequestMapping("/users")
public class UserRestController extends zamola.onboardingmanagement.RestController<User> {
    public UserRestController(final Service<User> service) {
        super(service);
    }
}
