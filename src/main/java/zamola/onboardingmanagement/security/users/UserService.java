package zamola.onboardingmanagement.security.users;

import org.springframework.stereotype.Service;
import zamola.onboardingmanagement.Repository;

@Service
public class UserService extends zamola.onboardingmanagement.Service<User> {
    public UserService(final Repository<User> repository) {
        super(repository);
    }
}
