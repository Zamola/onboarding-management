package zamola.onboardingmanagement.security.users;

import zamola.onboardingmanagement.Repository;

public interface UserRepository extends Repository<User> {
}
