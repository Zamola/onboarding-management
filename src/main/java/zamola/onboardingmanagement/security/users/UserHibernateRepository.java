package zamola.onboardingmanagement.security.users;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class UserHibernateRepository implements UserRepository {
    private static final String ID = "id";

    private final EntityManager entityManager;

    @Override
    public void createOrUpdate(final User user) {
        final Session session = getSession();
        session.saveOrUpdate(user);
    }

    @Override
    public Set<User> readAll() {
        final Session session = getSession();
        final Query<User> query = session.createQuery("from User", User.class);
        return query.getResultStream().collect(Collectors.toSet());
    }

    @Override
    public User read(final long id) {
        final Session session = getSession();
        return session.get(User.class, id);
    }

    @Override
    public void delete(final long id) {
        final Session session = getSession();
        final Query query = session.createQuery("delete from User where id=:" + ID);
        query.setParameter(ID, id);
        query.executeUpdate();
    }

    private Session getSession() {
        return entityManager.unwrap(Session.class);
    }
}
