package zamola.onboardingmanagement.security.roles;

import zamola.onboardingmanagement.Repository;

public interface RoleRepository extends Repository<Role> {
}
