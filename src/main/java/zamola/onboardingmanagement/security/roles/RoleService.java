package zamola.onboardingmanagement.security.roles;

import org.springframework.stereotype.Service;
import zamola.onboardingmanagement.Repository;

@Service
public class RoleService extends zamola.onboardingmanagement.Service<Role> {
    public RoleService(final Repository<Role> repository) {
        super(repository);
    }
}
