package zamola.onboardingmanagement.security.roles;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zamola.onboardingmanagement.Service;

@RestController
@RequestMapping("/roles")
public class RoleRestController extends zamola.onboardingmanagement.RestController<Role> {
    public RoleRestController(final Service<Role> service) {
        super(service);
    }
}
