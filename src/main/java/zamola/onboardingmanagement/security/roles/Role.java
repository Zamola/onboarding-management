package zamola.onboardingmanagement.security.roles;

import lombok.Data;
import zamola.onboardingmanagement.security.privileges.Privilege;
import zamola.onboardingmanagement.security.users.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
@Data
public class Role {
    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true, nullable = false)
    private String name;

    @ManyToMany
    private Set<Privilege> privileges;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;
}
