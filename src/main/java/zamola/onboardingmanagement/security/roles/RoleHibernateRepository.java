package zamola.onboardingmanagement.security.roles;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class RoleHibernateRepository implements RoleRepository {
    private static final String ID = "id";

    private final EntityManager entityManager;

    @Override
    public void createOrUpdate(final Role role) {
        final Session session = getSession();
        session.saveOrUpdate(role);
    }

    @Override
    public Set<Role> readAll() {
        final Session session = getSession();
        final Query<Role> query = session.createQuery("from Role", Role.class);
        return query.getResultStream().collect(Collectors.toSet());
    }

    @Override
    public Role read(final long id) {
        final Session session = getSession();
        return session.get(Role.class, id);
    }

    @Override
    public void delete(final long id) {
        final Session session = getSession();
        final Query query = session.createQuery("delete from Role where id=:" + ID);
        query.setParameter(ID, id);
        query.executeUpdate();
    }

    private Session getSession() {
        return entityManager.unwrap(Session.class);
    }
}
